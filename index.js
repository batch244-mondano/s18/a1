/*

	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

console.log("Hello world!");

// 1.
	// Addition
	function addTotal(firstNumber, secondNumber){
		console.log('Dispayed sum of ' + firstNumber + ' and ' + secondNumber);
		console.log(firstNumber+secondNumber);
	};
	addTotal(5,15);

	// Subtraction
	function subtractTotal(Number1, Number2){
		console.log('Dispayed difference of ' + Number1 + ' and ' + Number2);
		console.log(Number1-Number2);
	};
	subtractTotal(20,5);

// 2.
	// Multiplication
	function returnTimes(multiplier,multiplicand){
		console.log('The product of ' + multiplier + ' and ' + multiplicand + ':');
		let multiplied = multiplier*multiplicand;
		return multiplied;
	};
	let product = returnTimes(50,10);
		console.log(product);

	// Division
	function returnDivide(dividend,divisor){
		console.log('The quotient of ' + dividend + ' and ' + divisor + ':');
		let divided = dividend/divisor;
		return divided;
	};
	let quotient = returnDivide(50,10);
		console.log(quotient);

// 3. Area of Circle
	function returnCircle(radius){
		console.log('The result of getting the area of a circle with ' + radius + ' radius:');
		let areaCircle = 3.141592653589793238*radius*radius;
		return areaCircle;
	};
	let cicleArea = returnCircle(15);
		console.log(cicleArea);

// 4. Average of 4 numbers
	function getAverage(num1,num2,num3,num4){
		console.log('The average of ' + num1 +','+ num2+',' + num3+ ' and '+ 'num4' + ':');
		let ave4 = (num1+num2+num3+num4)/4;
		return ave4;
	};
	let averageVar = getAverage(20,40,60,80);
		console.log(averageVar);

// 5. Passing Score
	function checkPassing(score,total){
		console.log('Is ' + score +'/'+ total+ ' a passing score?');
		let percentageScore = (score/total)*100;
		let isPassed = percentageScore > 75;
		return isPassed;
	};
	let isPassingScore = checkPassing(38,50);
		console.log(isPassingScore);

